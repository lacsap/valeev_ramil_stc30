package WorkHome14;

import WorkHome14.models.ModelCar;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;


public class Main {

    public static void main(String[] args) {

        System.out.println();
        System.out.println("Номера всех автомобилей, имеющих черный цвет или нулевой пробег.");
        try {
            Files.lines(Paths.get("Base_car.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new ModelCar(Long.parseLong(array[0]), array[1], array[2], array[3],
                            Integer.parseInt(array[4]), Integer.parseInt(array[5])))
                    .filter(ModelCar -> (ModelCar.getColorCar().equals("black")) || (ModelCar.getMileageCar() == 0))
                    .forEach(System.out::println);
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }

        System.out.println();
        System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.");

        try {
            System.out.println(Files.lines(Paths.get("Base_car.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new ModelCar(Long.parseLong(array[0]), array[1], array[2], array[3],
                            Integer.parseInt(array[4]), Integer.parseInt(array[5])))
                    .filter(ModelCar -> (ModelCar.getPriceCar() >= 700000))
                    .filter(ModelCar -> (ModelCar.getPriceCar() <= 800000))
                    .map(ModelCar::getModelCar).distinct().count());


        } catch (IOException e) {
            throw new IllegalArgumentException();
        }

        System.out.println();
        System.out.println("Вывести цвет автомобиля с минимальной стоимостью");

        try {
            System.out.println(
                    Files.lines(Paths.get("Base_car.txt"))
                            .map(line -> line.split(" "))
                            .map(array -> new ModelCar(Long.parseLong(array[0]), array[1], array[2], array[3],
                                    Integer.parseInt(array[4]), Integer.parseInt(array[5])))
                            .min(Comparator.comparing(ModelCar::getPriceCar)).get().getColorCar());

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }

        System.out.println();
        System.out.println("Среднюю стоимость Camry.");
        try {
            System.out.println(Files.lines(Paths.get("Base_car.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new ModelCar(Long.parseLong(array[0]), array[1], array[2], array[3],
                            Integer.parseInt(array[4]), Integer.parseInt(array[5])))
                    .filter(ModelCar -> (ModelCar.getModelCar().equals("camry")))
                    .mapToInt(ModelCar::getPriceCar).average().getAsDouble());
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}



