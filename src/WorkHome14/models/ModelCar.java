package WorkHome14.models;

import java.util.Objects;

public class ModelCar {
    Long ID;
    String numberCar;
    String modelCar;
    String colorCar;
    Integer mileageCar;
    Integer priceCar;

    public ModelCar() {}

    public ModelCar(Long ID, String numberCar, String modelCar, String colorCar, Integer mileageCar, Integer priceCar) {
        this.ID = ID;
        this.numberCar = numberCar;
        this.modelCar = modelCar;
        this.colorCar = colorCar;
        this.mileageCar = mileageCar;
        this.priceCar = priceCar;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getNumberCar() {
        return numberCar;
    }

    public void setNumberCar(String numberCar) {
        this.numberCar = numberCar;
    }

    public String getModelCar() {
        return modelCar;
    }

    public void setModelCar(String modelCar) {
        this.modelCar = modelCar;
    }

    public String getColorCar() {
        return colorCar;
    }

    public void setColorCar(String colorCar) {
        this.colorCar = colorCar;
    }

    public Integer getMileageCar() {
        return mileageCar;
    }

    public void setMileageCar(Integer mileageCar) {
        this.mileageCar = mileageCar;
    }

    public Integer getPriceCar() {
        return priceCar;
    }

    public void setPriceCar(Integer priceCar) {
        this.priceCar = priceCar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelCar modelCar1 = (ModelCar) o;
        return Objects.equals(ID, modelCar1.ID) &&
                Objects.equals(numberCar, modelCar1.numberCar) &&
                Objects.equals(modelCar, modelCar1.modelCar) &&
                Objects.equals(colorCar, modelCar1.colorCar) &&
                Objects.equals(mileageCar, modelCar1.mileageCar) &&
                Objects.equals(priceCar, modelCar1.priceCar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, numberCar, modelCar, colorCar, mileageCar, priceCar);
    }

    @Override
    public String toString() {
        return "ModelCar{" +
                "ID=" + ID +
                ", numberCar='" + numberCar + '\'' +
                ", modelCar='" + modelCar + '\'' +
                ", colorCar='" + colorCar + '\'' +
                ", mileageCar=" + mileageCar +
                ", priceCar=" + priceCar +
                '}';
    }
}
