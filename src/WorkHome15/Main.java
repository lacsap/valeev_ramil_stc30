package WorkHome15;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;


public class Main {
    public static void main(String[] args) {
        String[] firstArray = new String[200];
        int bufferI = 0;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("Text.txt"));
            String current = bufferedReader.readLine();
            int i = 0;

            while (current != null) {
                firstArray[i] = current;
                i++;
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            bufferI = i;

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        String[] array = new String[bufferI];
        for (int j = 0; j != bufferI; j++) {
            array[j] = firstArray[j];
        }

        QuickSort quickSort = new QuickSort();
        System.out.println("--------Исходный массив--------");
        System.out.println(Arrays.toString(array));
        quickSort.quickSort(array);
        System.out.println("--------Отсортированный массив--------");
        System.out.println(Arrays.toString(array));

    }
}