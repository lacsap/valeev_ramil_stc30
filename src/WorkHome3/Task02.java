package WorkHome3;
//*Реализовать приложение, рассчитывающее определенный интеграл методом Симпсона* НЕ РЕШЕН/

public class Task02 {
    public static double f(double x){                                               //Функция синуса
        return Math.sin(x);
    }

    public static double integralBySimpson(double a, double b, int n) {
        double h = (b - a) /2* n;
        double result = 0;
        double sum0=0;
        double sum1=0;
        double sum2=0;

        for (double x = a; x <= b; x = x + h) {  //1   10
            sum1=f(x);
            sum0=f(x-1);
            sum2=f(x+1);
            result = result +(sum0+4*sum1+sum2);
        }

        return result = result*(h/3);
    }


    public static void printIntegralResultsForN(double a, double b, int ns[]) {
        for (int i = 0; i < ns.length; i++) {
            System.out.println("For N = " + ns[i]
                    + ", result = " + integralBySimpson(a, b, ns[i]));
        }
    }

    public static void main(String[] args) {
        int ns[] = {1, 10, 1_000, 10_000, 100_000, 1000_000};

        printIntegralResultsForN(0, 10, ns);
    }
}
