package WorkHome3;
//*Реализовать приложение,  со следующим набором функций и процедур:
//●    выводит сумму элементов массива
//●     выполняет разворот массива (массив вводится с клавиатуры).
//●    вычисляет среднее арифметическое элементов массива (массив вводится с
//клавиатуры).
//●    меняет местами максимальный и минимальный элементы массива
//●    выполняет сортировку массива методом пузырька.
//●    выполняет преобразование массива в число. */


import java.util.Scanner;
import java.util.Arrays;

public class Task01 {
    public static void main(String args[]) {
        int numberOperation;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину вводимого массива:");
        int n = scanner.nextInt();                                                     //длина массива
        int array[] = new int[n];                                                      //создание массива длиной n
        System.out.println("Введите " + n + "  значении в массив");
        for (int i = 0; i != array.length; i += 1) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Введите номер требуемой операции:");
        System.out.println("1 - Вывести сумму элементов массива;");
        System.out.println("2 - Выполнить разворот массива;");
        System.out.println("3 - Вычислить среднее арифметическое элементов массива;");
        System.out.println("4 - Поменять местами максимальный и минимальный элемент;");
        System.out.println("5 - Выполнить сортировку массива методом пузырька;");
        System.out.println("6 - Выполнить преобразования массива в число.");
        numberOperation = scanner.nextInt();
        switch (numberOperation) {
            case (1):
                sumArrayElements(array);
                break;
            case (2):
                turnArray(array);
                System.out.println(Arrays.toString(array));
                break;
            case (3):
                arrayAverage(array);
                break;
            case (4):
                replaceElement(array);
                break;
            case (5):
                shortBubble(array);
                break;
            case (6):
                System.out.println("Число преобразованное из массива:  " + transformerArray(array));
                break;
            default:
                System.err.println("Введено неверное значение операции.");

        }
    }                                                                                    // завершение метода точки входа

    public static void sumArrayElements(int array[]) {                                  //сумма элементов массива
        int arraySum = 0;
        for (int i = 0; i != array.length; i += 1) {
            arraySum = arraySum + array[i];
        }
        System.out.println("Сумма элементов массива равна " + arraySum);
    }


    public static void turnArray(int array[]) {
        int bufferA, bufferB;
        for (int i = 0; i < (array.length / 2); i += 1) {                              // переворачиваем массив
            bufferA = array[i];
            bufferB = array[array.length - 1 - i];
            array[i] = bufferB;
            array[array.length - 1 - i] = bufferA;
        }
    }

    public static void arrayAverage(int array[]) {                                      //среднее арифметическое
        float arrayAv = 0;
        for (int i = 0; i != array.length; i += 1) {
            arrayAv = arrayAv + array[i];
            arrayAv = arrayAv / array.length;
        }
        System.out.println(arrayAv);
    }

    public static void replaceElement(int array[]) {                                  //Замена максимального и мимнмального элемента массива
        int maxIn = 0;
        int minIn = 0;
        int minIndex = 0;
        int maxIndex = 0;
        minIn = array[0];
        for (int i = 0; i != array.length; i += 1) {
            if (array[i] < minIn) {
                minIn = array[i];
                minIndex = i;
            }
        }
        maxIn = array[0];
        for (int i = 0; i != array.length; i += 1) {
            if (array[i] > maxIn) {
                maxIn = array[i];
                maxIndex = i;
            }
        }
        array[minIndex] = maxIn;
        array[maxIndex] = minIn;
        System.out.println("Результат замены максимального и минимального элемента в массиве:");
        System.out.println(Arrays.toString(array));
    }

    public static void shortBubble(int array[]) {                                     //метод пузырьковой сортировки
        int bufferA = 0;
        for (int i = 0; i != array.length; i += 1) {
            for (int j = array.length - 1; j != 0; j -= 1) {
                if (array[j] < array[j - 1]) {
                    bufferA = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = bufferA;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static int transformerArray(int array[]) {                                  //преобразование массива в число
        int bufferA = 0;
        int number = 0;
        for (int i = 0; i != array.length; i += 1) {
            bufferA = 1;
            for (int j = array.length - i - 1; j != 0; j -= 1) {
                bufferA = bufferA * 10;
            }
            number = number + array[i] * bufferA;
        }
        return number;
    }
}                                                                                      // завершение класса
