package WorkHome9;

public class NumberAndStringProcessor {
    private String arrayString[];
    private int arrayInt[];

    public NumberAndStringProcessor(String[] arrayString, int[] arrayInt) {
        this.arrayString = arrayString;
        this.arrayInt = arrayInt;
    }

    public String[] processString(StringsProcess method) {
        String[] result = new String[arrayString.length];
        for (int i = 0; i < arrayString.length; i++) {
            result[i] = method.process(arrayString[i]);
        }
        return result;
    }

    public int[] processInt(NumbersProcess method) {
        int result[] = new int [arrayInt.length];
        for (int i = 0; i < arrayInt.length; i++) {
            result[i] = method.process(arrayInt[i]);
        }
        return result;
    }

}
