package WorkHome9;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String arrayStringExample[] = {"Тор8т", "Ябло77ко", "Пи77цц8а", "Шелд99он", "Дороти", "Театр", "Мир"};
        int arrayIntExample[] = {120, 701, 7809, 456, 316050889, 4506, 852, 999};

        NumberAndStringProcessor example = new NumberAndStringProcessor(arrayStringExample, arrayIntExample);

        System.out.println("Исходный массив: " + Arrays.toString(arrayIntExample));
        System.out.println("---------------------------------------------------------");


        NumbersProcess numbersProcess;
        numbersProcess = (method) -> {
            int i = 1;
            int temp = 0;
            int count = 0;
            int buffer = method;
            while (buffer != 0) {
                buffer = buffer / 10;
                count++;                                                         //количество цифр в числе
            }

            while (count != 0) {
                i = (int) Math.pow(10, count - 1);
                temp = temp + method % 10 * i;                                         // разворот числа
                method = method / 10;
                count = count - 1;
            }
            return temp;
        };
        System.out.println("Разворот числа: " + Arrays.toString(example.processInt(numbersProcess)));


        numbersProcess = (method) -> {
            int i = 1;
            int temp = 0;
            int count = 0;
            int buffer = method;

            while (buffer != 0) {
                buffer = buffer / 10;
                count++;                                                         //количество цифр в числе
            }

            while (count != 0) {
                if (method % 10 != 0) {
                    temp = temp + method % 10 * i;                               // собираем число без 0
                    i = i * 10;
                }
                method = method / 10;
                count = count - 1;
            }
            return temp;
        };
        System.out.println("Исключение 0: " + Arrays.toString(example.processInt(numbersProcess)));



        numbersProcess = (method) -> {
            int i = 1;
            int temp = 0;
            int count = 0;
            int buffer = method;
            while (buffer != 0) {
                buffer = buffer / 10;
                count++;                                                         //количество цифр в числе
            }

            while (count != 0) {

                if (method % 2 == 0) {
                    temp = temp + (((method % 10 )-1)* i);
                    if (temp==-1){
                        temp=9;                                                 //0 меняем на 9 рассматриваю цифры от 0 .. 9, ближайший слева полуачется снова 9
                    }
                    i = i * 10;
                }
                else {
                    temp = temp + method % 10 * i;
                    i = i * 10;
                }
                method = method / 10;
                count = count - 1;
            }
            return temp;
        };
        System.out.println("Четные числа -1 (0 заменяется на 9): " + Arrays.toString(example.processInt(numbersProcess)));





        System.out.println();
        System.out.println();
        System.out.println("Исходный массив: " + Arrays.toString(arrayStringExample));
        System.out.println("---------------------------------------------------------");


        StringsProcess stringsProcess;

        stringsProcess=(method)->{
            char[] in = method.toCharArray();
            int begin = 0;
            int end = in.length - 1;
            char temp;
            while (end > begin) {
                temp = in[begin];
                in[begin] = in[end];
                in[end] = temp;
                end--;
                begin++;
            }
            return new String(in);
        };

        System.out.println("Разворот строки: " + Arrays.toString(example.processString(stringsProcess)));



        stringsProcess=(method)->{
            char[] in = method.toCharArray();
            int begin = 0;
            int i = 0;
            int count = 0;
            int end = in.length;

            while (end > begin) {
                if (!Character.isDigit(in[begin])) {
                    count++;                                    //количество символов не цифр
                }
                begin++;
            }
            char bufferIn[] = new char[count];                  //создали новый массив для необходимого кол-ва символов не цифр
            begin = 0;
            end = in.length;
            while (end > begin) {
                if (!Character.isDigit(in[begin])) {
                    bufferIn[i] = in[begin];                    //заполнили новый массив символовами не цифр
                    i++;
                }
                begin++;
            }
            return new String(bufferIn);
        };

        System.out.println("Исключение цифр: " +  Arrays.toString(example.processString(stringsProcess)));


        stringsProcess=(method)->{
            return method.toUpperCase();
        };

        System.out.println("С заглавной буквы: " + Arrays.toString(example.processString(stringsProcess)));

    }
}
