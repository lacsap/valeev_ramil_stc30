package WorkHome1;

public class Task02 {
    public static void main(String[] args) {
        int inA = 55653;
        String buffB = "";
        while (inA > 0) {
            buffB = buffB + inA % 2;
            inA = inA / 2;
        }

        for (int i = buffB.length() - 1; i >= 0; i--) {
            System.out.print(buffB.charAt(i));
        }
    }
}

