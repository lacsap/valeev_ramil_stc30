package WorkHome7;

public class Main {
    public static void main(String[] args) {
        Human exampleIvanov = new Human.Builder()
                .withFirstName("Ivanov")
                .withLastName("Ivan")
                .withAge(45)
                .withFinishedCourse(true)
                .Build();


        Human examplePetrov = new Human.Builder()
                .withFirstName("Petrov")
//                .withLastName("Petr")
                .withAge(35)
//                .withFinishedCourse(false)
                .Build();


        System.out.println(exampleIvanov.getFirstName());
        System.out.println(exampleIvanov.getLastName());
        System.out.println(exampleIvanov.getAge());
        System.out.println();
        System.out.println(examplePetrov.getFirstName());
        System.out.println(examplePetrov.getAge());


    }


}
