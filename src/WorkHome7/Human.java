package WorkHome7;

public class Human {
    String firstName;
    String lastName;
    int age;
    boolean isFinishedCourse;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isFinishedCourse() {
        return isFinishedCourse;
    }

    public static class Builder {
        private Human newHuman;

        public Builder() {
            newHuman = new Human();
        }

        public Builder withFirstName(String firstName) {
            newHuman.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName) {
            newHuman.lastName = lastName;
            return this;
        }

        public Builder withAge(int age) {
            newHuman.age = age;
            return this;
        }

        public Builder withFinishedCourse(boolean isFinish) {
            newHuman.isFinishedCourse = isFinish;
            return this;
        }

        public Human Build() {
            return newHuman;
        }
    }
}