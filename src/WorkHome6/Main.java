package WorkHome6;


import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        String commandButton;
        int numberButtom;
        boolean onTV = false;
        TV tv = new TV("Самсунг");
        RemoteController remoteController = new RemoteController(tv);

        Channel ch1 = new Channel("Первый канал");
        tv.addProgramList(ch1);
        ch1.takeProgramToChanel("Новости");
        ch1.takeProgramToChanel("Смак");
        ch1.takeProgramToChanel("Поле чудес");
        ch1.takeProgramToChanel("В мире животных");
        ch1.takeProgramToChanel("Голос");
        ch1.takeProgramToChanel("Непутевые заметки");

        Channel ch2 = new Channel("Пятница");
        tv.addProgramList(ch2);
        ch2.takeProgramToChanel("Школа доктора Комаровского");
        ch2.takeProgramToChanel("Орел и решка");
        ch2.takeProgramToChanel("Адская кухня");
        ch2.takeProgramToChanel("Ревизоро");
        ch2.takeProgramToChanel("Мир наизнанку");
        ch2.takeProgramToChanel("Магаззино");

        Channel ch3 = new Channel("ТНТ");
        tv.addProgramList(ch3);
        ch3.takeProgramToChanel("Перезагрузка");
        ch3.takeProgramToChanel("Дом2");
        ch3.takeProgramToChanel("Где логика");
        ch3.takeProgramToChanel("Импровизация");
        ch3.takeProgramToChanel("Золото геленджика");
        ch3.takeProgramToChanel("StandUp");

        Channel ch4 = new Channel("СТС");
        tv.addProgramList(ch4);
        ch4.takeProgramToChanel("Шоу уральских пельменей");
        ch4.takeProgramToChanel("Кухня");
        ch4.takeProgramToChanel("Отель Элеон");
        ch4.takeProgramToChanel("Рогов студия");
        ch4.takeProgramToChanel("6 Кадров");
        ch4.takeProgramToChanel("Форд Боярд");


        Scanner scanner = new Scanner(System.in);
        System.out.println("Для включения ТВ введите + ");
        commandButton = scanner.nextLine();
        if (commandButton.equals("+")) {
            tv.turnOnTV();
            onTV = true;
        }


        while (onTV) {
            System.out.println("Для просмотра списка программ введите *");
            System.out.println("Для просмотра просмотра программы передач введите / ");
            System.out.println("Для выбора канала нажмите номер канала ");
            System.out.println("Для отключения ТВ нажмите - ");
            System.out.println();
            commandButton = scanner.nextLine();

            switch (commandButton) {
                case ("*"):
                    remoteController.listChannel();
                    System.out.println();
                    break;
                case ("/"):
                    remoteController.getNameCannel(1);
                    remoteController.programListChannel(1);

                    remoteController.getNameCannel(2);
                    remoteController.programListChannel(2);

                    remoteController.getNameCannel(3);
                    remoteController.programListChannel(3);

                    remoteController.getNameCannel(4);
                    remoteController.programListChannel(4);

                    System.out.println();
                    break;
                case ("-"):
                    onTV = false;
                    System.out.println();
                    break;
                case ("1"):
                    remoteController.currentNumberChannel(1);
                    System.out.println();
                    break;
                case ("2"):
                    remoteController.currentNumberChannel(2);
                    System.out.println();
                    break;
                case ("3"):
                    remoteController.currentNumberChannel(3);
                    System.out.println();
                    break;
                case ("4"):
                    remoteController.currentNumberChannel(4);
                    System.out.println();
                    break;

                default:
                    System.err.println("Ведена неверная команда попробуйте снова");
            }
        }
    }
}