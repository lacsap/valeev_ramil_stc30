package WorkHome6;



public class Channel {
    private String nameChannel;
    private String programList[];
    private int programCounter;

    public Channel(String nameChannel) {
        this.nameChannel = nameChannel;
        programList = new String[6];
    }

    public String getNameChannel() {
        return nameChannel;
    }

    public void takeProgramToChanel(String nameProgram) {                           //заполнение списка программы передач
        Program program = new Program(nameProgram);
        this.programList[programCounter] = program.getNameProgram();
        this.programCounter++;
    }


    public String[] getProgramList() {                                              //возврат полного списка передач
        return programList;
    }

    public String getProgramName(int randomNumber) {                                //возврат случайной программы канала
        String nameProgram = this.programList[randomNumber];
        return nameProgram;
    }

}