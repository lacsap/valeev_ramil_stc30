package WorkHome6;


import java.util.Random;

public class RemoteController {
    private int numberChanel;
    private TV tv;


    public RemoteController(TV tv) {
        this.tv = tv;
        takeTV(tv);
    }


    public void currentNumberChannel(int numberChanel) {                       // Задание номера канала
        this.numberChanel = numberChanel;
        tv.getNameChanel(numberChanel - 1);
        int number = numberRandomProgram();
        tv.getProgramName(numberChanel - 1, numberRandomProgram());
    }

    public int getNumberChanel() {
        return numberChanel;
    }

    public int numberRandomProgram() {                                      //Выбор случайного номера программы
        Random random = new Random();
        return random.nextInt(7 - 1);
    }

    public void takeTV(TV tv) {
        this.tv = tv;
    }

    public void programListChannel(int numberChanel) {                      //Показать список программ канала
        tv.getChanelProgramList(numberChanel - 1);
    }

    public void listChannel() {                                            //Показать список каналов
        tv.getProgramList();
    }

    public void getNameCannel(int numberChanel) {
        tv.getNameChanel(numberChanel - 1);
    }

}
