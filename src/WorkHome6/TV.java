package WorkHome6;

import java.util.Arrays;

public class TV {
    private String nameTV;
    public Channel channelList[];
    public int channelCount;


    public TV(String nameTV) {
        this.nameTV = nameTV;
        this.channelList = new Channel[4];
    }

    public void addProgramList(Channel channel) {                                                                                       // заполнение списка каналов
        this.channelList[channelCount] = channel;
        this.channelCount++;
    }

    public void getProgramList() {                                                                                                     // печать списка каналов
        System.out.println("Список каналов: ");

        for (int i = 0; i != channelList.length; i++) {
            System.out.println((i + 1) + ". " + channelList[i].getNameChannel());
        }
        return;
    }


    public void getNameChanel(int numberChannel) {                                                                                      // печать название канала
        System.out.println("Текущий канал: " + channelList[numberChannel].getNameChannel());
    }

    public void getChanelProgramList(int numberChannel) {                                                                              //печать программ передач канала
        System.out.println("Список программ текущего канала: " + Arrays.toString(channelList[numberChannel].getProgramList()));
    }

    public void getProgramName(int numberChannel, int randomNumber) {                                                                  //печать случайной программы канала
        System.out.println("Текущая программа канала: " + "<" + channelList[numberChannel].getProgramName(randomNumber) + ">");
    }

    public void turnOnTV() {
        System.out.println("/////////// Телевизор включен ///////////");

    }
}





