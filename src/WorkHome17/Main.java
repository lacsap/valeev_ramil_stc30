package WorkHome17;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;


public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        int count = random.nextInt(100);
        int array[] = new int[count];
        AtomicInteger summArray= new AtomicInteger();

        for (int i = 0; i != count; i++) {
            array[i] = 1;
        }
        ThreadService thread1 = new ThreadService();
        thread1.submit(() -> {
            int summ=0;
            for (int i = 0; i < array.length/3; i++) {
              summ+=array[i];
            }
            summArray.addAndGet(summ);
            System.out.println(Thread.currentThread().getName() + ": from " + "0" + " to " + array.length/3  + " sum is " + summ );  //Thread 1: from 0 to 4 sum is 5
        });

        ThreadService thread2 = new ThreadService();
        thread2.submit(() -> {
            int summ=0;
            for (int i = (array.length/3); i < (array.length/3)*2; i++) {
                summ+=array[i];
            }
            summArray.addAndGet(summ);
            System.out.println(Thread.currentThread().getName() + ": from " + ((array.length/3)) + " to " + (array.length/3)*2  + " sum is " + summ );  //Thread 1: from 0 to 4 sum is 5
        });

        ThreadService thread3 = new ThreadService();
        thread3.submit(() -> {
            int summ=0;
            for (int i = (array.length/3)*2; i < array.length; i++) {
                summ+=array[i];
            }
            summArray.addAndGet(summ);
            System.out.println(Thread.currentThread().getName() + ": from " + ((array.length/3)*2) + " to " + (array.length)  + " sum is " + summ );  //Thread 1: from 0 to 4 sum is 5
        });

        System.out.println("Sum by threads: " + summArray);
        System.out.println("array.length " + array.length);

    }
}



