package WorkHome8;

public interface Relocatable {
    float coordinateDx(float coordinateDx);

    float coordinateDy(float coordinateDy);
}
