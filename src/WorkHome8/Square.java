package WorkHome8;


public class Square extends GeometricFigures implements Scaling, Relocatable {
    public double side;


    public Square(float coordinateX, float coordinateY, float side) {
        super(coordinateX, coordinateY);
        this.side = side;
    }


    @Override
    double getArea() {
        return side * side;
    }

    @Override
    double getPerimeter() {
        return 4 * side;
    }

    @Override
    public float coordinateDx(float coordinateDx) {
        return this.coordinateX += coordinateDx;
    }

    @Override
    public float coordinateDy(float coordinateDy) {
        return this.coordinateY += coordinateDy;
    }

    @Override
    public double coeffScalling(float coeffScalling) {
        return this.side = this.side * coeffScalling;
    }


}
