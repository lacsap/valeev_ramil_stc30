package WorkHome8;


abstract class GeometricFigures {
    public float coordinateX;
    public float coordinateY;


    public GeometricFigures(float coordinateX, float coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    abstract double getArea();

    abstract double getPerimeter();


}
