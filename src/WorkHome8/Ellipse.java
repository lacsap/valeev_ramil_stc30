package WorkHome8;

import java.lang.Math;

public class Ellipse extends GeometricFigures implements Scaling, Relocatable {
    double majorAxis;
    double minorAxis;

    public Ellipse(float coordinateX, float coordinateY, double majorAxis, double minorAxis) {
        super(coordinateX, coordinateY);
        this.majorAxis = majorAxis;
        this.minorAxis = minorAxis;
    }


    @Override
    double getArea() {
        return Math.PI * majorAxis * minorAxis;
    }

    @Override
    double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((Math.pow(majorAxis, 2) + Math.pow(minorAxis, 2)) / 2);
    }

    @Override
    public float coordinateDx(float coordinateDx) {
        return this.coordinateX += coordinateDx;
    }

    @Override
    public float coordinateDy(float coordinateDy) {
        return this.coordinateY += coordinateDy;
    }

    @Override
    public double coeffScalling(float coeffScalling) {
        coeffScallingMinor(coeffScalling);
        return this.majorAxis = this.majorAxis * coeffScalling;
    }

    private double coeffScallingMinor(float coeffScalling) {
        return this.minorAxis = this.minorAxis * coeffScalling;

    }

}
