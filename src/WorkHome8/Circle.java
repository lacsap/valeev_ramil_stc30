package WorkHome8;


import java.lang.Math;

public class Circle extends GeometricFigures implements Scaling, Relocatable {
    public double radius;

    public Circle(float coordinateX, float coordinateY, float radius) {
        super(coordinateX, coordinateY);
        this.radius = radius;
    }

    @Override
    double getArea() {
        return Math.pow(radius, 2) * Math.PI;
    }

    @Override
    double getPerimeter() {
        return 2 * Math.PI * radius;
    }


    @Override
    public float coordinateDx(float coordinateDx) {
        return this.coordinateX += coordinateDx;
    }

    @Override
    public float coordinateDy(float coordinateDy) {
        return this.coordinateY += coordinateDy;
    }

    @Override
    public double coeffScalling(float coeffScalling) {
        return this.radius = this.radius * coeffScalling;
    }
}
