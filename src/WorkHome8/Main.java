package WorkHome8;


public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(1, 1, 1);
        System.out.println("----------------------Круг----------------------");
        System.out.println("Координаты фигуры X/Y: " + circle.coordinateX + " / " + circle.coordinateY);
        System.out.println("Радиус фигуры: " + circle.radius);
        System.out.printf("Площадь фигуры: " + "%5.3f%n", circle.getArea());
        System.out.printf("Периметр фигуры: " + "%5.3f%n", circle.getPerimeter());
        System.out.println();
        circle.coordinateDx(10);
        circle.coordinateDy(10);
        circle.coeffScalling(2);
        System.out.println("Координаты фигуры после смещения X/Y: " + circle.coordinateX + " / " + circle.coordinateY);
        System.out.printf("Площадь фигуры после масштабирования: " + "%5.3f%n", circle.getArea());
        System.out.printf("Периметр фигуры после масштабирования: " + "%5.3f%n", circle.getPerimeter());
        System.out.println();

        System.out.println("----------------------Квадрат----------------------");
        Square square = new Square(1, 1, 5);
        System.out.println("Координаты фигуры X/Y: " + square.coordinateX + " / " + square.coordinateY);
        System.out.println("Сторона фигуры: " + square.side);
        System.out.printf("Площадь фигуры: " + "%5.3f%n", square.getArea());
        System.out.printf("Периметр фигуры: " + "%5.3f%n", square.getPerimeter());
        square.coordinateDx(10);
        square.coordinateDy(10);
        square.coeffScalling(2);
        System.out.println("Координаты фигуры после смещения X/Y: " + square.coordinateX + " / " + square.coordinateY);
        System.out.printf("Площадь фигуры после масштабирования: " + "%5.3f%n", +square.getArea());
        System.out.printf("Периметр фигуры после масштабирования: " + "%5.3f%n", +square.getPerimeter());
        System.out.println();

        System.out.println("----------------------Прямоугольник----------------------");
        Rectangle rectangle = new Rectangle(1, 1, 5, 10);
        System.out.println("Координаты фигуры X/Y: " + rectangle.coordinateX + " / " + rectangle.coordinateY);
        System.out.println("Стороны фигуры высота/длина: " + rectangle.height + " / " + rectangle.width);
        System.out.printf("Площадь фигуры: " + "%5.3f%n", rectangle.getArea());
        System.out.printf("Периметр фигуры: " + "%5.3f%n", rectangle.getPerimeter());
        rectangle.coordinateDx(10);
        rectangle.coordinateDy(10);
        rectangle.coeffScalling(2);
        System.out.println("Координаты фигуры после смещения X/Y: " + rectangle.coordinateX + " / " + rectangle.coordinateY);
        System.out.printf("Площадь фигуры после масштабирования: " + "%5.3f%n", +rectangle.getArea());
        System.out.printf("Периметр фигуры после масштабирования: " + "%5.3f%n", +rectangle.getPerimeter());
        System.out.println();

        System.out.println("----------------------Элипс----------------------");
        Ellipse ellipse = new Ellipse(1, 1, 10, 5);
        System.out.println("Координаты фигуры X/Y: " + ellipse.coordinateX + " / " + ellipse.coordinateY);
        System.out.println("Стороны фигуры  длинная полуось/короткая полуось: " + ellipse.majorAxis + " / " + ellipse.minorAxis);
        System.out.printf("Площадь фигуры: " + "%5.3f%n", ellipse.getArea());
        System.out.printf("Периметр фигуры: " + "%5.3f%n", ellipse.getPerimeter());
        ellipse.coordinateDx(10);
        ellipse.coordinateDy(10);
        ellipse.coeffScalling(2);
        System.out.println("Координаты фигуры после смещения X/Y: " + ellipse.coordinateX + " / " + ellipse.coordinateY);
        System.out.printf("Площадь фигуры после масштабирования: " + "%5.3f%n", +ellipse.getArea());
        System.out.printf("Периметр фигуры после масштабирования: " + "%5.3f%n", +ellipse.getPerimeter());
        System.out.println();
    }

}
