package WorkHome8;

public class Rectangle extends GeometricFigures implements Scaling, Relocatable {
    public float height;
    public float width;

    public Rectangle(float coordinateX, float coordinateY, float heigth, float widht) {
        super(coordinateX, coordinateY);
        this.height = heigth;
        this.width = widht;

    }

    @Override
    double getArea() {
        return height * width;
    }

    @Override
    double getPerimeter() {
        return 2 * (height + width);
    }


    @Override
    public float coordinateDx(float coordinateDx) {
        return this.coordinateX += coordinateDx;
    }

    @Override
    public float coordinateDy(float coordinateDy) {
        return this.coordinateY += coordinateDy;
    }

    @Override
    public double coeffScalling(float coeffScalling) {
        coeffScallingWidht(coeffScalling);
        return this.height = this.height * coeffScalling;
    }

    private double coeffScallingWidht(float coeffScalling) {
        return this.width = this.width * coeffScalling;

    }

}
