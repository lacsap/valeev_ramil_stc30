package WorkHome10;


public class MainArrayList {
    public static void main(String[] args) {
        List list = new ArrayList();

        for (int i = 0; i < 15; i++) {
            list.add(i);
        }

        System.out.println(" Work iterator");
        System.out.println();
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-----------------------------------");

        System.out.println();
        System.out.println(" list.insert(999,6)");
        System.out.println();
        list.insert(999, 6);
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("--------------------------------");



        System.out.println(" list.removeFirst(10);");
        System.out.println();
        list.removeFirst(10);
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("--------------------------------");


        System.out.println(" list.removeByIndex(1);");
        System.out.println();
        list.removeByIndex(1);
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("--------------------------------");


        System.out.println(" list.reverse()");
        System.out.println();
        list.reverse();
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("--------------------------------");



    }



}

