package WorkHome10;

public interface List extends Collection {
    int get(int index);

    int indexOF(int element);

    void removeByIndex(int index);

    void insert(int element, int index);

    void reverse();
}
