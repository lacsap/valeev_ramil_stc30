package WorkHome10;


public class LinkedList implements List {

    private class LinkedListIterator implements Iterator {
        private Node nodeCurrent = first;

        @Override
        public int next() {

            int value = nodeCurrent.value;
            nodeCurrent = nodeCurrent.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return nodeCurrent != null;
        }
    }

    private Node first;
    private Node last;
    private int count;


    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void reverse() {
        int i = 1;
        int j = 1;
        int keypadCount = count;
        int newCount = count;
        Node nodeNew = last;
        Node nodePrevius = first;

        while (j != keypadCount) {
            i = 1;
            nodePrevius = first;
            while (i != newCount - 1) {
                nodePrevius = nodePrevius.next;                     //находим предпоследний элемент
                i++;
            }
            this.add(nodePrevius.value);                            //добаляем в конец списка
            j++;
            newCount--;
        }
        while (i != keypadCount) {
            nodePrevius = nodePrevius.next;
            i++;
        }
        first = nodePrevius;                                        //делаем началом списка конец предыдущего списка
        count=count/2;

    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }

        System.err.println("Такого элемента нет");
        return -1;
    }

    @Override
    public int indexOF(int element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }


    @Override
    public boolean contains(int element) {
        return indexOF(element) != -1;
    }


    @Override
    public void insert(int element, int index) {
        int i = 1;
        Node nodeCurrent = first;
        if (index > count) {
            System.err.println("Введен несуществующий индекс");
        } else {
            if (index == 1) {
                Node nodeNew = new Node(element);
                nodeNew.next = nodeCurrent;
                first = nodeNew;
                count++;
            } else {
                while (i != index - 1) {
                    nodeCurrent = nodeCurrent.next;
                    i++;
                }
                Node nodeNew = new Node(element);
                nodeNew.next = nodeCurrent.next;
                nodeCurrent.next = nodeNew;
                count++;
            }
        }
    }


    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }


    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        Node nodeCurrent = first;
        Node nodePrevius = null;
        if (contains(element)) {

            while (nodeCurrent.value != element) {
                nodePrevius = nodeCurrent;
                nodeCurrent = nodeCurrent.next;
            }
            if (nodePrevius != null) {
                nodePrevius.next = nodeCurrent.next;
            } else {
                first = nodeCurrent.next;
            }
            count--;
        } else {
            System.err.println("Данного элемента нет в списке");
        }
    }


    @Override
    public void removeByIndex(int index) {
        int i = 0;
        Node nodeCurrent = first;
        int element = 0;
        if (index > count) {
            System.err.println("Введен несуществующий индекс");
        } else {
            while (i != index) {
                element = nodeCurrent.value;
                nodeCurrent = nodeCurrent.next;
                i++;
            }
            removeFirst(element);
        }
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }


}
