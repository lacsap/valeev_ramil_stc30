package WorkHome16.BinaryTree;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;


public class BinarySearchTreeImpl<T extends Comparable<T>> implements BinarySearchTree<T> {

    static class Node<E> {
        E value;
        Node<E> left;
        Node<E> right;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        if (root == null) {
            root = new Node<>(value);
        }
        // значение меньше корня
        else if (value.compareTo(root.value) < 0) {
            // добавляем в левое поддерево
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }

        return root;
    }

    @Override
    public void printDfs() {

        Queue<Node<T>> currentLevel = new LinkedList<Node<T>>();
        Queue<Node<T>> nextLevel = new LinkedList<Node<T>>();

        currentLevel.add(root);

        while (!currentLevel.isEmpty()) {
            Iterator<Node<T>> iter = currentLevel.iterator();
            while (iter.hasNext()) {
                Node <T> currentNode = iter.next();
                if (currentNode.left != null) {
                    nextLevel.add(currentNode.left);
                }
                if (currentNode.right != null) {
                    nextLevel.add(currentNode.right);
                }
                System.out.print(currentNode.value + " ");
            }
            System.out.println();
            currentLevel = nextLevel;
            nextLevel = new LinkedList<Node<T>>();

        }
    }



    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        stack.push(root);

        Node<T> current;

        while (!stack.isEmpty()) {
            current = stack.pop();
            System.out.println(current.value + " ");
            if (current.left != null) {
                stack.push(current.left);
            }
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    @Override
    public void printBfs() {

        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);

        Node<T> current;

        while (!queue.isEmpty()) {
            current = queue.poll();
            System.out.println(current.value + " ");
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }


    }

    private void dfs(Node<T> root) {
        System.out.println("in root = " + root);
        if (root != null) {
            dfs(root.left);
            System.out.println(root.value + " ");
            dfs(root.right);
        }
        System.out.println("from root = " + root);
    }

    @Override
    public void remove(T value) {
        Node<T> preCurrentValue = null;
        Node<T> currentValue = null;
        Node<T> bufferValue = root;

        while (bufferValue != null) {
            if (bufferValue.value.compareTo(value) == 0) {
                currentValue = bufferValue;
                break;
            } else {
                if (bufferValue.value.compareTo(value) < 1) {
                    preCurrentValue = bufferValue;

                    bufferValue = bufferValue.right;
                }
            }
            if (bufferValue.value.compareTo(value) == 0) {
                currentValue = bufferValue;
                break;
            } else {
                if (bufferValue.value.compareTo(value) > -1) {
                    preCurrentValue = bufferValue;
                    bufferValue = bufferValue.left;
                }
            }
        }
        // У удаляемого узла нет right
        if (currentValue.right == null) {
            preCurrentValue.left = currentValue.left;
        }
        // У удаляемого узла есть right у которого в свою очередь нет left
        if (currentValue.right != null) {
            if (currentValue.left == null) {
                preCurrentValue.right = currentValue.right;
            }

        // У удаляемого узла есть right у которого в свою очередь нет right

            if (currentValue.right != null) {
                Node<T> buffer = currentValue.right;

                if (buffer.right == null) {
                    Node<T> buffer1 = currentValue.left;//2
                    Node<T> buffer2 = buffer.left;//6
                    buffer.left=null;
                    buffer2.right=buffer; //6-->7
                    buffer2.left=buffer1; //6-->2
                    preCurrentValue.left  = buffer2;

                }
            }
        }

        return;
    }

    @Override
    public boolean contains(T value) {
        Node<T> bufferValue = root;
        while (bufferValue != null) {
            if (bufferValue.value.equals(value)) {
                return true;
            } else {
                if (bufferValue.value.compareTo(value) > -1) {
                    bufferValue = bufferValue.left;
                }
                if (bufferValue.value.compareTo(value) < 1) {
                    bufferValue = bufferValue.right;
                }
            }
        }

        return false;
    }
}
