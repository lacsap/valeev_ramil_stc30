package WorkHome16.BinaryTree;

public class Main {

    public static void main(String[] args) {
        BinarySearchTree<Integer> tree = new BinarySearchTreeImpl<>();
        tree.insert(8);
        tree.insert(3);
        tree.insert(10);
        tree.insert(1);
        tree.insert(6);
        tree.insert(14);
        tree.insert(4);
        tree.insert(7);
        tree.insert(13);


        System.out.println(tree.contains(144));
        System.out.println(tree.contains(14));
        System.out.println();
        tree.printDfs();
        tree.remove(13);
        System.out.println();
        tree.printDfs();


    }
}
