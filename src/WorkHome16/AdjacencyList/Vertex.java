package WorkHome16.AdjacencyList;

/**
 * 02.12.2020
 * 33. Graphs
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Vertex { //вершина
    int getNumber(); //номер вершины
}
