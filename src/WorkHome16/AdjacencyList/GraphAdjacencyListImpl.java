package WorkHome16.AdjacencyList;


import java.util.Arrays;
import java.util.LinkedList;


public class GraphAdjacencyListImpl implements OrientedGraph {

    private static final int MAX_VERTICES_COUNT = 5;
    private int count = 0;
    LinkedList<Integer> grafList = new LinkedList<Integer>();
    int index = 0;
    private LinkedList matrix[] = new LinkedList[MAX_VERTICES_COUNT];

    public GraphAdjacencyListImpl() {
        for (int i = 0; i != MAX_VERTICES_COUNT; i++) {
            LinkedList<Integer> pull = new LinkedList<>();
            matrix[i] = pull;
        }
    }


    @Override
    public void addVertex(Vertex vertex) {
        count++;
    }

    @Override
    public void addEdge(Edge edge) {
        for (int i = 0; i != MAX_VERTICES_COUNT; i++) {
            if (i == edge.getFirst().getNumber()) {
                matrix[i].add(edge.getSecond().getNumber());
            }
        }


    }

    @Override
    public boolean isAdjacent(Vertex a, Vertex b) {
        return false;
    }

    @Override
    public void print() {

        for (int i = 0; i < matrix.length; i++) {
            System.out.println(i + " --> " + Arrays.toString(matrix[i].stream().toArray()) + " ");
        }

    }

    @Override
    public boolean searchDFS(int element) {
        int i = 0;
        LinkedList buffer = new LinkedList();
        int j = 0;
        int seachVericle = 0;
        while (j != matrix.length - 1) {
            i = j;
            while (i != matrix.length) {
                buffer = matrix[i];
                if (buffer.size() != 0) {
                    seachVericle = Integer.parseInt(buffer.get(0).toString());
                    if (seachVericle == element) {
                        return true;
                    } else {
                        i = Integer.parseInt(buffer.get(0).toString());
                    }
                } else {
                    j++;
                    break;
                }
            }
        }
        return false;
    }

    @Override
    public boolean searchBFS(int element) {
        int i = 0;
        LinkedList buffer = new LinkedList();
        int j = 0;
        int x = 0;
        int seachVericle = 0;
        while (j != matrix.length - 1) {
            i = j;
            x=0;
            while (i != matrix.length) {
                buffer = matrix[i];
                if (buffer.size() != 0) {
                    while (x != buffer.size()) {
                        seachVericle = Integer.parseInt(buffer.get(x).toString());
                        if (seachVericle == element) {
                            return true;
                        } else {
                            x++;
                        }
                    }

                }
                j++;
                break;

            }
        }
        return false;
    }
}
