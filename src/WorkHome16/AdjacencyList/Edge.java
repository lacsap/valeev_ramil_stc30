package WorkHome16.AdjacencyList;

/**
 * 02.12.2020
 * 33. Graphs
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Edge { //ребро
    Vertex getFirst();  //первый край
    Vertex getSecond(); //второй край
    int weight(); //вес
}
