package WorkHome16.AdjacencyList;

/**
 * 02.12.2020
 * 33. Graphs
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface OrientedGraph {
    void addVertex(Vertex vertex); //добавить вершину
    void addEdge(Edge edge);//добавить ребро
    boolean isAdjacent(Vertex a, Vertex b); //связь между вершинами
    void print();
    boolean searchDFS(int element);
    boolean searchBFS(int element);
}
