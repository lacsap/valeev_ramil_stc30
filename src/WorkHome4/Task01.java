package WorkHome4;

//*Реализовать Фибоначчи с однократным вызовом рекурсии. */

import java.util.Scanner;

public class Task01 {
    public static int fib(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }

        int array[] = new int[n];
        array[0] = 1;
        array[1] = 1;
        int i = 2;
        return fibArray(array, n, i);
    }

    public static int fibArray(int array[], int n, int i) {

        if (i < n) {
            array[i] = array[i - 1] + array[i - 2];
            i += 1;
            fibArray(array, n, i);
        }
        return array[n - 1];
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите порядковый номер элемента Фибоначчи:");
        int numberFib = scanner.nextInt();
        System.out.println("Сумма для порядкового номера элемента Фибоначи " + numberFib + " равна: " + fib(numberFib));
    }
}
