package WorkHome4;
//* Реализовать метод бинарного поиска с помощью рекурсии. */


import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class Task02_1 {


    public static void shortBubble(int array[]) {                                                       //метод пузырьковой сортировки
        int bufferA = 0;
        for (int i = 0; i != array.length; i += 1) {
            for (int j = array.length - 1; j != 0; j -= 1) {
                if (array[j] < array[j - 1]) {
                    bufferA = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = bufferA;
                }
            }
        }
    }

    public static String binarySearch(int array[], int numberForSearch, int left, int right) {           // рекурсионный бинарный поиск

        if (left > right) {
            return "Требуемый элемент НЕ НАЙДЕН";
        }

        int middle = (right + left) / 2;

        if (array[middle] > numberForSearch) {
            return binarySearch(array, numberForSearch, left, middle - 1);
        }

        if (array[middle] < numberForSearch) {
            return binarySearch(array, numberForSearch, middle + 1, right);

        }

        return "Требуемый элемент НАЙДЕН";

    }


    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину заполняемого массива (массив заполнится генератором случайных чисел):");
        int n = scanner.nextInt();
        int array[] = new int[n];
        Random random = new Random();
        for (int i = 0; i != array.length; i += 1) {
            array[i] = random.nextInt(500 + 1);
        }
        System.out.println(Arrays.toString(array));

        System.out.println("Введите значения для поиска в массиве:");
        int numberForSearch = scanner.nextInt();

        int left = 0;
        int right = array.length - 1;

        shortBubble(array);

        System.out.println(binarySearch(array, numberForSearch, left, right));
    }


}
