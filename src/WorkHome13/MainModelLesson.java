package WorkHome13;


import WorkHome13.dao.ModelLessonDAO;
import WorkHome13.dao.ModelLessonDaoFileImpl;
import WorkHome13.models.ModelLesson;
import WorkHome13.utils.IdGeneratorFileBasedImpl;

import java.util.Scanner;

public class MainModelLesson {
    public static void main(String[] args) {
        boolean startKey = true;
        Scanner scanner = new Scanner(System.in);

        ModelLessonDAO modelLessonDAO =
                new ModelLessonDaoFileImpl("Lessons.txt",
                        new IdGeneratorFileBasedImpl("Lessons_sequence.txt"));

        while (startKey){
            System.out.println("Введите nameLesson:");
            String nameLesson = scanner.nextLine();
            System.out.println("Введите dataLesson:");
            String dataLesson = scanner.nextLine();
            System.out.println("Введите nameCourse:");
            String nameCourse = scanner.nextLine();
            ModelLesson modelLesson = new ModelLesson(nameLesson, dataLesson, nameCourse);
            modelLessonDAO.save(modelLesson);

            System.out.println("Повторить ввод? Y/N");
            String command = scanner.nextLine();

            if (command.equals("N")) {
                startKey = false;;
            };
        }

        System.out.println(modelLessonDAO.findAll().toString());
        System.out.println(modelLessonDAO.findByID(2L).toString());
        System.out.println(modelLessonDAO.findByName("English").toString());
        System.out.println(modelLessonDAO.findByName("Not").toString());
        }

    }



