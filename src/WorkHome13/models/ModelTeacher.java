package WorkHome13.models;



import java.util.ArrayList;
import java.util.Objects;


public class ModelTeacher {
    private Long ID;
    private String firstName;
    private String lastName;
    private Integer workTime;
    private ArrayList<String> listCourse;

    public ModelTeacher() {}

    public ModelTeacher(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public ModelTeacher(Long ID, String firstName, String lastName, Integer workTime, ArrayList<String> listCourse) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.workTime = workTime;
        this.listCourse = listCourse;
   }
    public ModelTeacher(String firstName, String lastName, Integer workTime, ArrayList<String> listCourse) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.workTime = workTime;
        this.listCourse = listCourse;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getWorkTime() {
        return workTime;
    }

    public void setWorkTime(Integer workTime) {
        this.workTime = workTime;
    }

    public ArrayList<String> getListCourse() {
        return listCourse;
    }

    public void setListCourse(ArrayList<String> listCourse) {
        this.listCourse = listCourse;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelTeacher that = (ModelTeacher) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(workTime, that.workTime) &&
                Objects.equals(listCourse, that.listCourse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, workTime, listCourse);
    }

    @Override
    public String toString() {
        return "ModelTeacher{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", workTime=" + workTime +
                ", listCourse=" + listCourse.toString() + '}';
    }
}
