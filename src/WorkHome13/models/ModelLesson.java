package WorkHome13.models;


import java.util.Objects;

public class ModelLesson {
    private Long ID;
    private String nameLesson;
    private String dataLesson;
    private String nameCourse;

    public ModelLesson() {};

    public ModelLesson(String nameLesson, String dataLesson, String nameCourse) {
        this.nameLesson = nameLesson;
        this.dataLesson = dataLesson;
        this.nameCourse = nameCourse;
    }

    public ModelLesson(String nameLesson, String nameCourse) {
        this.nameLesson = nameLesson;
        this.nameCourse = nameCourse;
    }

    public ModelLesson(Long ID, String nameLesson, String dataLesson, String nameCourse) {
        this.ID = ID;
        this.nameLesson = nameLesson;
        this.dataLesson = dataLesson;
        this.nameCourse = nameCourse;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }

    public String getNameLesson() {
        return nameLesson;
    }

    public void setNameLesson(String nameLesson) {
        this.nameLesson = nameLesson;
    }

    public String getDataLesson() {
        return dataLesson;
    }

    public void setDataLesson(String dataLesson) {
        this.dataLesson = dataLesson;
    }

    public String getCourse() {
        return nameCourse;
    }

    public void setCourse(String course) {
        nameCourse = nameCourse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelLesson that = (ModelLesson) o;
        return Objects.equals(nameLesson, that.nameLesson) &&
                Objects.equals(dataLesson, that.dataLesson) &&
                Objects.equals(nameCourse, that.nameCourse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameLesson, dataLesson, nameCourse);
    }

    @Override
    public String toString() {
        return "ModelLesson{" +
                "ID=" + ID +
                ", nameLesson='" + nameLesson + '\'' +
                ", dataLesson='" + dataLesson + '\'' +
                ", nameCourse='" + nameCourse + '\'' +
                '}';
    }
}