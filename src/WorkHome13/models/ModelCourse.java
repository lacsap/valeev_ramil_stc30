package WorkHome13.models;

import java.util.ArrayList;
import java.util.Objects;


public class ModelCourse {
    private Long ID;
    private String nameCourse;
    private String timeStartCourse;
    private String timeFinishCourse;
    private ArrayList<String> listTeacher;
    private ArrayList<String> listLessonCourse;


    public ModelCourse() {
    }

    public ModelCourse(String nameCourse, String timeStartCourse, String timeFinishCourse) {
        this.nameCourse = nameCourse;
        this.timeStartCourse = timeStartCourse;
        this.timeFinishCourse = timeFinishCourse;
    }

    public ModelCourse(Long ID, String nameCourse, String timeStartCourse,
                       String timeFinishCourse, ArrayList<String> listTeacher, ArrayList<String> listLessonCourse) {
        this.ID = ID;
        this.nameCourse = nameCourse;
        this.timeStartCourse = timeStartCourse;
        this.timeFinishCourse = timeFinishCourse;
        this.listTeacher = listTeacher;
        this.listLessonCourse = listLessonCourse;
    }

    public ModelCourse(String nameCourse, String timeStartCourse,
                       String timeFinishCourse, ArrayList<String> listTeacher, ArrayList<String> listLessonCourse) {
        this.nameCourse = nameCourse;
        this.timeStartCourse = timeStartCourse;
        this.timeFinishCourse = timeFinishCourse;
        this.listTeacher = listTeacher;
        this.listLessonCourse = listLessonCourse;
    }


    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }

    public String getTimeStartCourse() {
        return timeStartCourse;
    }

    public void setTimeStartCourse(String timeStartCourse) {
        this.timeStartCourse = timeStartCourse;
    }

    public String getTimeFinishCourse() {
        return timeFinishCourse;
    }

    public void setTimeFinishCourse(String timeFinishCourse) {
        this.timeFinishCourse = timeFinishCourse;
    }

    public ArrayList<String> getListTeacher() {
        return listTeacher;
    }

    public void setListTeacher(ArrayList<String> listTeacher) {
        this.listTeacher = listTeacher;
    }

    public ArrayList<String> getListLessonCourse() {
        return listLessonCourse;
    }

    public void setListLessonCourse(ArrayList<String> listLessonCourse) {
        this.listLessonCourse = listLessonCourse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelCourse that = (ModelCourse) o;
        return Objects.equals(ID, that.ID) &&
                Objects.equals(nameCourse, that.nameCourse) &&
                Objects.equals(timeStartCourse, that.timeStartCourse) &&
                Objects.equals(timeFinishCourse, that.timeFinishCourse) &&
                Objects.equals(listTeacher, that.listTeacher) &&
                Objects.equals(listLessonCourse, that.listLessonCourse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, nameCourse, timeStartCourse, timeFinishCourse, listTeacher, listLessonCourse);
    }

    @Override
    public String toString() {
        return "ModelCourse{" +
                "ID=" + ID +
                ", nameCourse='" + nameCourse + '\'' +
                ", timeStartCourse='" + timeStartCourse + '\'' +
                ", timeFinishCourse='" + timeFinishCourse + '\'' +
                ", listTeacher=" + listTeacher +
                ", listLessonCourse=" + listLessonCourse +
                '}';
    }
}
