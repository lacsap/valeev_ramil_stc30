package WorkHome13.dao;

import WorkHome13.models.ModelTeacher;

import java.util.ArrayList;
import java.util.Optional;

public interface ModelTeacherDAO extends CrudDAO <ModelTeacher>{
    Optional<ModelTeacher> findByName(String name);
    ArrayList<String> findByTeacher (String nameCourse);

}
