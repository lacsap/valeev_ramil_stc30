package WorkHome13.dao;

import WorkHome13.models.ModelCourse;
import WorkHome13.models.ModelTeacher;
import WorkHome13.utils.IdGenerator;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ModelCourseDaoFileImpl implements ModelCourseDAO {
    private String fileName;
    private IdGenerator idGenerator;



    private Mapper<ModelCourse, String> modelCourseToStringMapper = ModelCourse ->
            ModelCourse.getID() + " " +
                    ModelCourse.getNameCourse() + " " +
                    ModelCourse.getTimeStartCourse() + " " +
                    ModelCourse.getTimeFinishCourse() + " " +
                    ModelCourse.getListTeacher()+ " '' " +
                    ModelCourse.getListLessonCourse() + "\r\n";


    private Mapper<String, ModelCourse> stringToModelCourse = string -> {
        int startArrayTwo= 0;
        ArrayList<String> arrayListTeacher= new ArrayList<>();
        ArrayList<String> arrayListLesson=new ArrayList<>();
        String data[] = string.split("\\s*(\\s|[|]|\\,\\s)");

        for (int i=0; i!=data.length;i++){
            if (data[i].indexOf("''")!=-1){
                startArrayTwo=i;
            }
        }

        data[4]=data[4].replace("[", "");
        data[startArrayTwo-1]=data[startArrayTwo-1].replace("]", "");
        data[startArrayTwo+1]=data[startArrayTwo+1].replace("[", "");
        data[data.length-1]=data[data.length-1].replace("]", "");
        for (int i=4; i!= startArrayTwo;i++)       {
            data[i]=data[i].replace(",", "");
            arrayListTeacher.add(data[i]);
        }
        for (int i=startArrayTwo+1; i!= data.length;i++)       {
            data[i]=data[i].replace(",", "");
            arrayListLesson.add(data[i]);
        }

        return new ModelCourse(Long.parseLong(data[0]), data[1], data[2], data[3], arrayListTeacher, arrayListLesson);
    };

    public ModelCourseDaoFileImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }


    @Override
    public Optional<ModelCourse> findByName(String name) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                String existedFirstName = data[1];

                // если мы нашли такой firstName
                if (existedFirstName.equals(name)) {
                    ModelCourse modelCourse = stringToModelCourse.map(current);
                    return Optional.of(modelCourse);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<ModelCourse> findByID(Long ID) {

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                Long existedId = Long.parseLong(data[0]);

                // если мы нашли такой ИД
                if (existedId.equals(ID)) {
                    ModelCourse modelCourse = stringToModelCourse.map(current);
                    return Optional.of(modelCourse);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(ModelCourse entity) {
        try {
            entity.setID(idGenerator.nextId());
            // открываем поток для записи
            OutputStream outputStream = new FileOutputStream(fileName, true);
            // записали байты
            outputStream.write(modelCourseToStringMapper.map(entity).getBytes());
            // закрыли поток
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(ModelCourse entity) {

    }

    @Override
    public List<ModelCourse> findAll() {
        try {
            // создаем пустой список
            List<ModelCourse> modelCoursesList = new ArrayList<>();
            // открываем файл для чтения
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            // считываем первую строку
            String current = bufferedReader.readLine();
            // пока строка не null (т.е. есть что считывать!)
            while (current!= null) {
                // преобразуем по правилу строку в объект типа ModelLesson
                ModelCourse modelCourse = stringToModelCourse.map(current);
                // добавляем Lesson в список
                modelCoursesList.add(modelCourse);
                // считываем следующую строку в файле
                current = bufferedReader.readLine();
            }
            // закрываем файл из которого считывали данные
            bufferedReader.close();
            // возвращаем полученный список
            return modelCoursesList;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(ModelCourse entity) {

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelCourseDaoFileImpl that = (ModelCourseDaoFileImpl) o;
        return Objects.equals(fileName, that.fileName) &&
                Objects.equals(idGenerator, that.idGenerator) &&
                Objects.equals(modelCourseToStringMapper, that.modelCourseToStringMapper);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, idGenerator, modelCourseToStringMapper);
    }

    @Override
    public String toString() {
        return "ModelCourseDaoFileImpl{" +
                "fileName='" + fileName + '\'' +
                ", idGenerator=" + idGenerator +
                ", modelCourseToStringMapper=" + modelCourseToStringMapper +
                '}';
    }
}
