package WorkHome13.dao;

import WorkHome13.models.ModelLesson;
import WorkHome13.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ModelLessonDaoFileImpl implements ModelLessonDAO {
    private String fileName;
    private IdGenerator idGenerator;

    private Mapper<ModelLesson, String> modelLessonToStringMapper = ModelLesson ->
            ModelLesson.getID() + " " +
                    ModelLesson.getNameLesson() + " " +
                    ModelLesson.getDataLesson() + " " +
                    ModelLesson.getNameCourse() + "\r\n";


    private Mapper<String, ModelLesson> stringToModelLesson = string -> {
        String data[] = string.split(" ");
        return new ModelLesson(Long.parseLong(data[0]), data[1], data[2], data[3]);
    };

    public ModelLessonDaoFileImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public Optional<ModelLesson> findByName(String nameLesson) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                String existedName = data[1];

                // если мы нашли такой NameLesson
                if (existedName.equals(nameLesson)) {
                    ModelLesson modelLesson = stringToModelLesson.map(current);

                    return Optional.of(modelLesson);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public ArrayList<String> findByCourseName(String courseName) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            ArrayList<String> arrayListNameLesson = new ArrayList<>();
            while (current != null) {
                String data[] = current.split(" ");

                String existedName = data[3];

                // если мы нашли такой courseName
                if (existedName.equals(courseName)) {
                    arrayListNameLesson.add(data[1]);
                    ModelLesson modelLesson = stringToModelLesson.map(current);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return arrayListNameLesson;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public Optional<ModelLesson> findByID(Long ID) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                Long existedId = Long.parseLong(data[0]);

                // если мы нашли такой ИД
                if (existedId.equals(ID)) {
                    ModelLesson modelLesson = stringToModelLesson.map(current);
                    return Optional.of(modelLesson);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(ModelLesson entity) {
        try {
            entity.setID(idGenerator.nextId());
            // открываем поток для записи
            OutputStream outputStream = new FileOutputStream(fileName, true);
            // записали байты
            outputStream.write(modelLessonToStringMapper.map(entity).getBytes());
            // закрыли поток
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void delete(ModelLesson entity) {

    }

    @Override
    public List<ModelLesson> findAll() {
        try {
            // создаем пустой список
            List<ModelLesson> modelLessons = new ArrayList<>();
            // открываем файл для чтения
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            // считываем первую строку
            String current = bufferedReader.readLine();
            // пока строка не null (т.е. есть что считывать!)
            while (current != null) {
                // преобразуем по правилу строку в объект типа ModelLesson
                ModelLesson modelLesson = stringToModelLesson.map(current);
                // добавляем Lesson в список
                modelLessons.add(modelLesson);
                // считываем следующую строку в файле
                current = bufferedReader.readLine();
            }
            // закрываем файл из которого считывали данные
            bufferedReader.close();
            // возвращаем полученный список
            return modelLessons;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(ModelLesson entity) {

    }
}
