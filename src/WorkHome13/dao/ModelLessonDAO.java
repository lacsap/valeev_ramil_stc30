package WorkHome13.dao;

import WorkHome13.models.ModelLesson;

import java.util.ArrayList;
import java.util.Optional;

public interface ModelLessonDAO extends CrudDAO<ModelLesson> {
    Optional<ModelLesson> findByName(String name);
    ArrayList<String> findByCourseName(String courseName);
}
