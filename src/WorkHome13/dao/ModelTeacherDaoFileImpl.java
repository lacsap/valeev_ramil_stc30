package WorkHome13.dao;


import WorkHome13.models.ModelLesson;
import WorkHome13.models.ModelTeacher;
import WorkHome13.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ModelTeacherDaoFileImpl implements ModelTeacherDAO {
    private String fileName;
    private IdGenerator idGenerator;

    public ModelTeacherDaoFileImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<ModelTeacher, String> modelTeacherToStringMapper = ModelTeacher ->
            ModelTeacher.getID() + " " +
                    ModelTeacher.getFirstName() + " " +
                    ModelTeacher.getLastName() + " " +
                    ModelTeacher.getWorkTime() + " " +
                    ModelTeacher.getListCourse() + "\r\n";


    private Mapper<String, ModelTeacher> stringToModelTeacher = string -> {
        String data[] = string.split("\\s*(\\s|[|]|\\,\\s)");
              data[4] = data[4].replace("[", "");
            data[data.length-1] = data[data.length-1].replace("]", "");

            ArrayList<String> bufferArrayList = new ArrayList<>();
            for (int i = 4; i != data.length; i++) {
                data[i] = data[i].replace(",", "");
                bufferArrayList.add(data[i]);
            }
            return new ModelTeacher(Long.parseLong(data[0]), data[1], data[2], Integer.parseInt(data[3]), bufferArrayList);
    };

    @Override
    public Optional<ModelTeacher> findByName(String firstName) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                String existedFirstName = data[1];

                // если мы нашли такой firstName
                if (existedFirstName.equals(firstName)) {
                    ModelTeacher modelTeacher = stringToModelTeacher.map(current);
                    return Optional.of(modelTeacher);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public ArrayList<String> findByTeacher(String nameCourse) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            ArrayList<String> arrayListNameTeacher = new ArrayList<>();
            while (current!= null) {
                String data[] = current.split(" ");
                data[4]=data[4].replace("[", "");
                data[data.length-1]=data[data.length-1].replace("]", "");
                String existedName = data[3];

                for(int i=4;i!=data.length;i++){
                   data[i]=data[i].replace(",", "");
                   existedName = data[i];
                    // если мы нашли такой courseName
                    if (existedName.equals(nameCourse)) {
                        ModelTeacher modelTeacher = stringToModelTeacher.map(current);
                        arrayListNameTeacher.add(modelTeacher.getFirstName() + " " + modelTeacher.getLastName());
                    }
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return arrayListNameTeacher;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional findByID(Long ID) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                Long existedId = Long.parseLong(data[0]);

                // если мы нашли такой ИД
                if (existedId.equals(ID)) {
                    ModelTeacher modelTeacher = stringToModelTeacher.map(current);
                    return Optional.of(modelTeacher);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public void save(ModelTeacher entity) {
        try {
            entity.setID(idGenerator.nextId());
            // открываем поток для записи
            OutputStream outputStream = new FileOutputStream(fileName, true);
            // записали байты
            outputStream.write(modelTeacherToStringMapper.map(entity).getBytes());
            // закрыли поток
            outputStream.close();


        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(ModelTeacher entity) {

    }

    @Override
    public List findAll() {
        try {
            // создаем пустой список
            List<ModelTeacher> modelTeacherList = new ArrayList<>();
            // открываем файл для чтения
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            // считываем первую строку
            String current = bufferedReader.readLine();
            // пока строка не null (т.е. есть что считывать!)
            while (current != null) {
                // преобразуем по правилу строку в объект типа ModelLesson
                ModelTeacher modelTeacher = stringToModelTeacher.map(current);
                // добавляем Lesson в список
                modelTeacherList.add(modelTeacher);
                // считываем следующую строку в файле
                current = bufferedReader.readLine();
            }
            // закрываем файл из которого считывали данные
            bufferedReader.close();
            // возвращаем полученный список
            return modelTeacherList;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(ModelTeacher entity) {

    }
}
