package WorkHome13.dao;
// преобразует объект типа X в объект типа Y
public interface Mapper <X,Y>{
    Y map (X x);

}
