package WorkHome13.dao;

import WorkHome13.models.ModelCourse;

import java.util.Optional;

public interface ModelCourseDAO extends CrudDAO<ModelCourse>{
    Optional<ModelCourse> findByName(String name);
}
