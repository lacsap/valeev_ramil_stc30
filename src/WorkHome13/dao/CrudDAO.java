package WorkHome13.dao;

import java.util.List;
import java.util.Optional;

// CREATE, READ, UPDATE, DELETE
public interface CrudDAO<T> {
    Optional<T> findByID(Long ID);
    void save (T entity);
    void delete (T entity);
    List<T> findAll();
    void update (T entity);

}
