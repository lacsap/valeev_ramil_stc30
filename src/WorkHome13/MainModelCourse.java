package WorkHome13;

import WorkHome13.dao.*;
import WorkHome13.models.ModelCourse;
import WorkHome13.models.ModelTeacher;
import WorkHome13.utils.IdGeneratorFileBasedImpl;

import java.util.ArrayList;
import java.util.Scanner;

public class MainModelCourse {
    public static void main(String[] args) {
        boolean regKey = true;
        boolean startKey = true;
        Scanner scanner = new Scanner(System.in);
        ModelCourseDAO modelCourseDAO =
                new ModelCourseDaoFileImpl("Course.txt",
                        new IdGeneratorFileBasedImpl("Course_sequence.txt"));

        ModelLessonDAO modelLessonDAO =
                new ModelLessonDaoFileImpl("Lessons.txt",
                        new IdGeneratorFileBasedImpl("Lessons_sequence.txt"));


        ModelTeacherDAO modelTeacherDAO =
                new ModelTeacherDaoFileImpl("Teacher.txt",
                        new IdGeneratorFileBasedImpl("Teacher_sequence.txt"));


        while (startKey) {
            ArrayList<String> teacherArrayList = new ArrayList<>();
            ArrayList<String> lessonArrayList = new ArrayList<>();
            regKey = true;
            System.out.println("Введите nameCourse:");
            String nameCourse = scanner.nextLine();
            System.out.println("Введите timeStartCourse:");
            String timeStartCourse = scanner.nextLine();
            System.out.println("Введите timeFinishCourse:");
            String timeFinishCourse = scanner.nextLine();

            lessonArrayList = modelLessonDAO.findByCourseName(nameCourse);
            teacherArrayList = modelTeacherDAO.findByTeacher(nameCourse);
            ModelCourse modelCourse = new ModelCourse(nameCourse, timeStartCourse,
                    timeFinishCourse, teacherArrayList, lessonArrayList);

            modelCourseDAO.save(modelCourse);

            System.out.println("Повторить ввод? Y/N");
            String command2 = scanner.nextLine();
            if (command2.equals("N")) {
                startKey = false;
            }
            System.out.println(modelCourseDAO.findAll());
            System.out.println(modelCourseDAO.findByID(1L));
            System.out.println(modelCourseDAO.findByName("MBI"));
            System.out.println(modelCourseDAO.findByName("void"));
        }
    }
}

