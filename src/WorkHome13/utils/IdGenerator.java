package WorkHome13.utils;

public interface IdGenerator {
    Long nextId();
}
