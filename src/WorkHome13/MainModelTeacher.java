package WorkHome13;

import WorkHome13.dao.ModelTeacherDAO;
import WorkHome13.dao.ModelTeacherDaoFileImpl;
import WorkHome13.models.ModelTeacher;
import WorkHome13.utils.IdGeneratorFileBasedImpl;

import java.util.ArrayList;
import java.util.Scanner;

public class MainModelTeacher {
    public static void main(String[] args) {
        boolean regKey = true;
        boolean startKey = true;
        Scanner scanner = new Scanner(System.in);
        ModelTeacherDAO modelTeacherDAO =
                new ModelTeacherDaoFileImpl("Teacher.txt",
                        new IdGeneratorFileBasedImpl("Teacher_sequence.txt"));

        while (startKey) {
            ArrayList<String> bufferArrayList = new ArrayList<>();
            regKey = true;
            System.out.println("Введите firstName:");
            String firstName = scanner.nextLine();
            System.out.println("Введите lastName:");
            String lastName = scanner.nextLine();

            while (regKey) {
                System.out.println("Введите nameCourse:");
                String nameCourse = scanner.nextLine();
                bufferArrayList.add(nameCourse);
                System.out.println("Добавить еще курс Y/N");
                String command = scanner.nextLine();

                if (command.equals("N")) {
                    regKey = false;
                }
            }
            System.out.println("Введите стаж:");
            String workTime = scanner.nextLine();

            ModelTeacher modelTeacher = new ModelTeacher(firstName, lastName, Integer.parseInt(workTime), bufferArrayList);
            modelTeacherDAO.save(modelTeacher);

            System.out.println("Повторить ввод? Y/N");
            String command2 = scanner.nextLine();
            if (command2.equals("N")) {
                startKey = false;
            }
        }
        System.out.println(modelTeacherDAO.findByID(4L).toString());
        System.out.println(modelTeacherDAO.findAll().toString());
        System.out.println(modelTeacherDAO.findByName("Ivanov1"));
    }
}


