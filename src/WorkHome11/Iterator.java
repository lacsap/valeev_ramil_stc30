package WorkHome11;

public interface Iterator <A> {
    A next();

    boolean hasNext();
}
