package WorkHome11;

public interface Iterable <B> {
    Iterator <B> iterator();
}
