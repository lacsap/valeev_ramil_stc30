package WorkHome11;

public class MainLinkedList {
    public static void main(String[] args) {
        List list = new LinkedList();

        list.add(	'\u0006');
        list.add("sTRING");
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(	false);
        list.add('\u0006');
        list.add(8);


        System.out.println(" Work iterator");
        System.out.println();
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-----------------------------------");


        System.out.println("list.removeByIndex(1)");
        System.out.println();
        list.removeByIndex(1);
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-----------------------------------");


        System.out.println(" list.removeFirst(5);");
        System.out.println();
        list.removeFirst(5);
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-----------------------------------");

        System.out.println("list.insert(99,3);");
        System.out.println();
        list.insert(99,3);
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-----------------------------------");



        System.out.println(" list.contains(4)");
        System.out.println();
        System.out.println(list.contains(4));
        System.out.println("-----------------------------------");




        System.out.println("list.reverse()");
        System.out.println();
        list.reverse();

        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-----------------------------------");
    }



}
