package WorkHome11;

public interface List<D> extends Collection <D>{
    D get(int index);

    int indexOF(D element);

    void removeByIndex(int index);

    void insert(D element, int index);

    void reverse();
}
