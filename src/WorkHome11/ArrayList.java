package WorkHome11;


public class ArrayList <T> implements List <T> {
    private static final int DEFAULT_SIZE = 10;

    private T data[];
    private int count;

    public ArrayList() {
        this.data  = (T[])new Object[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator {

        private int current = 0;


        @Override
        public T next() {
            T value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public T get(int index) {
        if (index < count) {
            return this.data[index];
        } else {
            System.err.println("Вышли за предел массива");
            return null;
        }
    }


    @Override
    public int indexOF(T element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }


    @Override
    public boolean contains(T element) {
        return indexOF(element) != -1;
    }


    @Override
    public void removeByIndex(int index) {
        if (index<count){
        for (int i = index; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        count--;
        }else{
            System.err.println("Данный индекс не найден");
        }
    }

    @Override
    public void insert(T element, int index) {

        if (count == data.length - 1) {
            resize();
        }

        for (int i = count + 1; i > index; i--) {
            this.data[i] = this.data[i - 1];
        }
        count++;
        this.data[index] = element;
    }

    @Override
    public void reverse() {
        T temp=null;
        int counReverse = count;
    for (int i=0; i!=count/2; i++ ) {
        temp = this.data[i];
        counReverse--;
        this.data[i]=this.data[counReverse];
        this.data[counReverse]=temp;

    }


    }

    @Override
    public void add(T element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1);
        T newData[] = (T[])new Object[newLength];
        System.arraycopy(this.data, 0, newData, 0, oldLength);
        this.data = newData;
    }


    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(T element) {                                  //переделал в рамках создания removeByIndex избежать дублирования
        if (contains(element)) {
            removeByIndex(indexOF(element));
        } else {
            System.err.println("Данного элемент не найден");
        }
    }


//    @Override                                                             //исходный
//    public void removeFirst(int element) {
//        int indexOfRemovingElement = indexOF(element);
//        for (int i = indexOfRemovingElement; i < count - 1; i++) {
//            this.data[i] = this.data[i + 1];
//        }
//        count--;
//    }


    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}


