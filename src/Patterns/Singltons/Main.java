package Patterns.Singltons;

public class Main {
    public static void main(String[] args) {
        //создаем обьект singlton, сколько раз его создать вернеться единственный объект
        Singlton singlton = Singlton.newSinglton();
        //доступ к методу singltona
        singlton.info();
    }

}
