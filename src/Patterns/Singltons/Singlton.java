package Patterns.Singltons;


/**
 * Патерн Singlton (одиночка) необходим:
 * 1) Когда необходимо создать не более одного обьекта данного класса (например логгер)
 * 2) Когда необходимо создать глобальную точку доступа к классу (например доступ к базе)
 *
 */

public class Singlton {

    //создаем обьект типа Singlton
private final static Singlton instance;
    //инициализируем его
static {
    instance = new Singlton();
}

    //делаем приватный конструктор
    private Singlton () {};

   //создаем метод для создания Singlton и возвращаем единчтвенно созданный

    public static Singlton newSinglton (){
        return instance;
    };

    //добавляем по необходимости методы к Singlton

    public void info (){
        System.out.println("Вывод какой то инфы к примеру");
    }


}
