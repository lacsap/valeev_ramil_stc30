package Patterns.Builder;

/**
 * Паттерн Builder используеться создания сложного обьекта с использованием простых объектов
 */

public class Human {

    //* в качестве примера создадим некого Human c некоторыми полями

    String firstName;
    String lastName;

    //добавим геттеры для полей

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    //создадим сам класс Builder
    public static class Builder {
        //создаем объект типа Human
        private Human newHuman;

        //создаем конструктор для объекта
        public Builder() {
            newHuman = new Human();
        }

        // создадим метод класса builder
        public Builder withFirstName(String firstName) {
            newHuman.firstName = firstName;
            return this;
        };

        // создадим метод класса builder
        public Builder withLastName(String lastName) {
            newHuman.lastName= lastName;
            return this;
        };

        //Делаем Build
        public  Human Build (){
            return  newHuman;
        }
    }

}
