package Patterns.Builder;

public class Main {

    public static void main(String[] args) {
        Human example=new Human.Builder()
                .withFirstName("Petr")
                .withLastName("Petrov")
                .Build();
        System.out.println(example.getFirstName() + "   " + example.lastName);
    }
}
