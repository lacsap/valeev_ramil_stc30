package WorkHome2;
//*Реализовать приложение, которое вычисляет среднее арифметическое элементов массива (массив вводится с клавиатуры).*/

import java.util.Scanner;

class Task03 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длинну массива");
        int n = scanner.nextInt();                              //ввод длины массива
        int array[] = new int[n];                               // создание массива длины n
        float arrayAverage = 0;                                 // переменная среднее арифметическое


        for (int i = 0; i != array.length; i += 1) {                   // Заполняем массив данными + считаем сумму элементов
            array[i] = scanner.nextInt();
            arrayAverage = arrayAverage + array[i];
        }
        arrayAverage = arrayAverage / n;                            // находим арифметическое среднее

        System.out.println(arrayAverage);                       // вывод на консоль
    }
}

