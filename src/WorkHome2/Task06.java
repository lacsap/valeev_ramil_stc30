package WorkHome2;
//*Реализовать приложение, которое выполняет преобразование массива в число. /

public class Task06 {
    public static void main(String args[]) {

        int array[] = {4, 2, 3, 5, 7};
        int number = 0;
        int bufferA;
        for (int i = 0; i != array.length; i += 1) {
            bufferA = 1;
            for (int j = array.length - i - 1; j != 0; j -= 1) {        //расчет множителя
                bufferA = bufferA * 10;
            }
            number = number + array[i] * bufferA;                  //преобразование в число
        }

        System.out.println(number); // вывод результата
    }
}
