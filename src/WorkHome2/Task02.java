package WorkHome2;
//*Реализовать приложение, которое выполняет разворот массива (массив вводится с клавиатуры).*/

import java.util.Scanner;
import java.util.Arrays;


public class Task02 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длинну массива");
        int n = scanner.nextInt();                                  //ввод длины массива
        int array[] = new int[n];                                   // создание массива длины n
        int bufferA, bufferB;                                       // буферные переменные для перекладывания значения массивов
        System.out.println("Введите "+n+"  значении в массив");

        for (int i=0; i!=array.length; i+=1) {                      // заполняем массив данными
            array[i]=scanner.nextInt(); }

        for (int i=0; i<(n/2); i+=1) {                              // переворачиваем массив
           bufferA=array[i];
           bufferB=array[array.length-1-i];
           array[i]=bufferB;
           array[array.length-1-i]=bufferA;
             }

        System.out.println(Arrays.toString(array));                 // выводится массив в зеркальном представлении
    }
}
