package WorkHome2;

import java.util.Arrays;
import java.util.Scanner;

//*Реализовать приложение, которое меняет местами максимальный и минимальный элементы массива (массив вводится с клавиатуры).*/

public class Task04 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длинну массива:");
        int n = scanner.nextInt();                                  //ввод длины массива
        int array[] = new int[n];                                   // создание массива длины n
        int minIn=0, maxIn=0, minIndex=0, maxIndex=0;

        System.out.println("Введите значения в массив:");           // заполнение массива данными
        for (int i=0; i!=array.length;i+=1) {
            array[i]=scanner.nextInt();
        }

        minIn=array[0];                                             // поиск минимального элемента
        for (int i=0; i!=array.length; i+=1) {
            if (array[i] < minIn) { minIn = array[i]; minIndex = i;
            }
        }

        maxIn=array[0];                                             // поиск максимального элемента
        for (int i=0; i!=array.length; i+=1) {
            if (array[i]>maxIn) {maxIn=array[i]; maxIndex=i;}
        }

        array[minIndex]=maxIn;                                      //Замена максимального и мимнмального элемента массива
        array[maxIndex]=minIn;
        System.out.println("Результат замены максимального и минимального элемента в массиве:");
        System.out.println(Arrays.toString(array));                 //вывод массива с заменой переменных

    }
}