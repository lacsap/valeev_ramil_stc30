package WorkHome2;

//*Реализовать приложение, которое выводит сумму элементов массива (массив вводится с клавиатуры).*/
import java.util.Scanner;

class Task01 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину вводимого массива:");
        int n = scanner.nextInt();                                           //длина массива
        int array[] = new int[n];                                            //создание массива длиной n
        int arraySum = 0;                                                    //переменная результата суммы элементов массива
        System.out.println("Введите "+n+"  значении в массив");
        for (int i=0; i!=array.length; i+=1) {
            array[i]=scanner.nextInt();
            arraySum=arraySum+array[i];}

        System.out.println("Сумма элементов массива равна " + arraySum);
    }
}
