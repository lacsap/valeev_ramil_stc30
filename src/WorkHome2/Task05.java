package WorkHome2;
//*Реализовать приложение, которое выполняет сортировку массива методом пузырька.*/

import java.util.Scanner;
import java.util.Arrays;

public class Task05 {
    public static void main(String args[]) {
        int bufferA = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длинну массива: ");
        int n = scanner.nextInt();                                  // ввод длины массива
        int array[] = new int[n];                                  // создание массива длинной n
        for (int i = 0; i != n; i += 1) {                               // заполнение массива переменными
            array[i] = scanner.nextInt();
        }
        for (int i = 0; i != array.length; i += 1) {                      //метод пузырьковой сортировки
            for (int j = array.length - 1; j != 0; j -= 1) {
                if (array[j] < array[j - 1]) {
                    bufferA = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = bufferA;
                }
            }
        }
        System.out.println(Arrays.toString(array));              //вывод результата сортировки
    }
};

