package WorkHome12.hashset;

import WorkHome12.Map;
import WorkHome12.Set;
import WorkHome12.result.HashMapImpl;



public class HashSetImpl<V> implements Set<V> {
    V value;
    String defaultValue = "void";
    Map<V,String> map = new HashMapImpl<>();

    public HashSetImpl() {
     }

    @Override
    public void add(V value) {
        map.put(value,defaultValue);
    }

    @Override
    public boolean contains(V value) {

        return map.get(value)=="void";
    }


}
