package WorkHome12.hashset;

import WorkHome12.Set;

public class Main {

    public static void main(String[] args) {
        Set<String> set = new HashSetImpl<>();
        set.add("Moscow");
        set.add("London");
        System.out.println(set.contains("Moscow"));
        System.out.println(set.contains("Zanzibar"));
        System.out.println(set.contains("London"));
        System.out.println(set.contains("Detroit"));

        System.out.println("---------------------------");
        System.out.println();


        Set<Character> set2 = new HashSetImpl<>();

        set2.add('M');
        set2.add('L');
        System.out.println(set2.contains('M'));
        System.out.println(set2.contains('L'));
        System.out.println(set2.contains('D'));

        System.out.println("---------------------------");
        System.out.println();


        Set<Integer> set3 = new HashSetImpl<>();

        set3.add(3);
        set3.add(10);
        System.out.println(set3.contains(3));
        System.out.println(set3.contains(5));
        System.out.println(set3.contains(10));

    }
}
