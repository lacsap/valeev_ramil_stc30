package WorkHome12.result;

import WorkHome12.Map;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMapImpl<>();
        System.out.println();
        map.put("Марсель", 1);
        map.put("Марсель", 111);
        map.put("Денис", 2);
        map.put("Илья", 3);
        map.put("Неля", 4);
        map.put("Катерина", 5);
        map.put("Катерина", 55);
        map.put("Полина", 6);
        map.put("Регина", 7);
        map.put("Максим", 8);
        map.put("Максим", 888);
        map.put("Сергей", 9);
        map.put("Иван", 10);
        map.put("Виктор", 11);
        map.put("Виктор Александрович", 12);
        map.put("Виктор Александрович", 1212);




        System.out.println("------------------");


        System.out.println(map.get("Марсель"));
        System.out.println();
        System.out.println(map.get("Денис"));
        System.out.println();
        System.out.println(map.get("Илья"));
        System.out.println();
        System.out.println(map.get("Неля"));
        System.out.println();
        System.out.println(map.get("Катерина"));
        System.out.println();
        System.out.println(map.get("Полина"));
        System.out.println();
        System.out.println(map.get("Регина"));
        System.out.println();
        System.out.println(map.get("Максим"));
        System.out.println();
        System.out.println(map.get("Сергей"));
        System.out.println();
        System.out.println(map.get("Иван"));
        System.out.println();
        System.out.println(map.get("Виктор"));
        System.out.println();
        System.out.println(map.get("Виктор Александрович"));
        System.out.println();
        System.out.println(map.get("dvfdfdf"));
        System.out.println();


    }
}
