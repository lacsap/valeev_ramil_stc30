package WorkHome12.result;

import WorkHome12.Map;

import java.util.Objects;


/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class

HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    // TODO: предусмотреть ЗАМЕНУ ЗНАЧЕНИЯ
    // put("Марсель", 27);
    // put("Марсель", 28);
    // у меня сохранятся оба значения, вам нужно сделать, чтобы хранилось только одно
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        } else {
            // если уже в этой ячейке (bucket) уже есть элемент
            // запоминяем его
            MapEntry<K, V> current = entries[index];
            // теперь дойдем до последнего элемента в этом списке
            if (current.key.equals(key)){
                entries[index].value=value;}

            while (current.next != null) {
                if (current.key.equals(key)){
                    current.value=value;
                    current = current.next;
                }else{
                    current = current.next;
                }

            }
            // теперь мы на последнем элементе, просто добавим новый элемент в конец
            if (current.key.equals(key)){
                current.value=value;}
            current.next = newMapEntry;
        }
    }

    // TODO: реализовать
    @Override
    public V get(K key) {
        int index = key.hashCode() & (entries.length - 1);
        MapEntry<K, V> current = entries[index];
        if (current!=null) {
            if (current.key.equals(key)) {
                System.out.println("Key   " + current.key);
                return current.value;
            } else {
                while (current.next != null) {
                    current = current.next;
                    if (current.key.equals(key)) {
                        System.out.println("Key   " + current.key);
                        return current.value;
                    }
                }
                return null;
            }
        } else {
            System.out.println("Key   " + key);
            return null;}
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapEntry mapEntry = (MapEntry) o;
        return Objects.equals(mapEntry, mapEntry.key);
    }

}

